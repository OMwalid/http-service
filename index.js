require('dotenv').config();
const express = require('express');
const app = express();

app.get('/', (req, res) => {
  res.send('Hello upwork!');
});

const PORT = process.env.PORT || 8080;
console.log(PORT)
app.listen(PORT, () => {
  console.log(`Server listening on port ${PORT}...`);
});
